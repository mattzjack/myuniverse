# 流花第一

甲辰年四月

I intend to write more.

In fact I have intended this very thing before, and the action I took at that time was to write, with pen on paper, in my diary. I do keep a diary, but it's always lagging 2 months behind, and I had difficulty coming up with daily content. Meanwhile, I figured I had many thoughts inside my brain that were worth documenting, ergo it didn't seem like a bad idea to document such in my diary entries, though the thoughts were not necessarily specific to one date.

That went okay for a little bit. In particular, I enjoyed using a physical pen -- even more so a wooden pencil -- but as old-fashioned as I try to be, I do desire a few features of digital text: sharing, searching, perhaps editing. And, I hate to admit that from a utilitarian perspective, it does seem advantageous in this modern world to be able to show others how I write.

## for those technically inclined

Of course, writing digitally was the first thing I tried, before resorting to writing analogue. But being kind of a perfectionist / OCD-ist, I couldn't decide on the most perfect platform, tool, even language. Here're the boring details of my search.

I considered self-hosting a website, and using a static-site generator. It seems like a fun exercise, too. I tried to learn Rust and wrote a web server --which is conveniently available as an example in the official Rust Book -- but I found out that to implement everything I want, there's a lot of work. Then I found a Rust package that simply serves static files, and another one that forwards my local port to a port on some public website. (unfortunately I can't seem to find the name of either package anymore) I did some wiring, and it worked, and it was fair.

Not wanting to write CSS and HTML manually, I looked for a static-site generator. However, I couldn't find one that fits my exact taste, so I attempted to write one, and soon enough, I also decided it was too much work, and stopped.

I also considered tools like Blogspot, or Wordpress / Square Space, or Mastodon, or Typst PDF, but none of them seemed ideal. I do want to my essays to be very portable, and ideally the whole stack is open-source.

All that is to say, I think writing Markdown and hosting them on Git / GitLab seems like a good choice, at least for the moment. It's as portable as it gets, if I need to change my mind later, and, GitLab Markdown viewer looks good enough, available in dark mode.