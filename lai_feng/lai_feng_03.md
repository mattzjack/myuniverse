# 來風第三

甲辰年九月

## Freeing Space from my Raspi

I own a Raspi, and not too long ago, I started using it to run Immich, a self-hosted image / video server. Here's my setup:

* The 32GB secure-digital card contains the PostgreSQL database for Immich, alongside the operating system
* A 512GB external drive contains all the images & videos & derivative data, such as thumbnails & transcoded videos.
* Another external drive contains the backup of my images & videos, using `rsync` (`grsync`)
* I should really be backing up my database, too. It doesn't seem very complicated, and I'll get to that soon.

It's been going well, and I keep my Immich instance (running on Docker) up to date. Immich has surprisingly frequent releases!

A few days ago, I was trying to upgrade Immich via `docker compose pull`, when it failed with:

> no space left on device

2 possibilities entered my mind:

* The secure-digital card is quite small to start with. Maybe I put too many files in the system, outside of Immich?
* The Immich database may have gotten too big.

In attempt to find the culprit:

1. Checking `df -h` indeed shows 100% disk usage on my secure-digital card.
2. I ran `du -sh` on various folders, but couldn't really identify what went wrong. I had `rust` installed, which took around 1.2GB. A few other folders are taking at most 1GB, and they don't really sum anywhere close to 30GB.
3. The internet suggests it could be log files / journal files, but they seem to be rather small and under control on my Raspi.
4. ~~Out of desperation~~ As a sanity check, I uninstalled some packages such as Firefox and Mousepad (I mostly run it headless, so I don't need them anyway), which freed up less than 1GB according to `df -h`. 
5. I attempted various `apt` operations including `autoremove` and `purge`. It appears I've been doing a good job cleaning up packages, and no space was freed from this.
6. I wasn't sure how to check the size of the Immich database, but I followed the instructions to back it up, and the result file was only a few hundred KB. That seems to indicate that the database (uncompressed) isn't very big either. I have ~20000 images & videos, which shouldn't take that much database space.

I finally decided to carefully examine all my folders, one by one. After stopping my containers and unmounting my external drive, I looked at all folders under `/`, and found that Docker is taking 20GB (under `/var/lib`)! That's it!

I imagined, maybe like a virtual machine, Docker is hogging 20GB of space as reserved for its containers to use as the file system? I couldn't find any evidence of that. However, I did find a way to check how much space Docker is using, with `docker system df`, which reported that Images (not to be confused with Immich picuters) were taking up 20GB. Notice that it's Images, as opposed to Containers.

Running `docker image list` reveals that I have dozens of images for Immich containers -- most likely from previous versions. They are just living rent-free inside my secure-digital card. Running `docker image prune` got rid of them, and my space was free, at last!