# 來風第一

甲辰年四月

## Motivation

I had an interesting exchange among friends: "I played Catan online against strangers and lost." "That's normal. Strangers are usually very good."

Are strangers very good? I'd like to try to answer this using what I know about data science.

Let's say being "good" means being an experienced player, one who has played the game many times.

Let's model the number of games played by each player by some distribution. In my head, I think there are a ton of players who play only 1 game ever, so the mode should be 1. Then, the probability mass function should go down as the number of games increases. Let's choose a distribution, say, Pareto distribution. I choose Pareto, because I recall learning about it in my Probability-&-Computing class, and about it having a long tail and an expectation of infinity. I thought that might be a good characterisation of the players (and even if not, we can always -- and I intend to -- switch to other distributions, and see how the outcomes differ).

## Digression: the Whatever-Zjack Distribution

Quick searching shows Pareto is a continuous distribution, and its discrete counter part is the ζ distribution, about which I know nothing, and which is apparently also not included in the Julia Distributions package. I looked at its definitions and plotted its probability mass function, but its relation to the Pareto distribution is not immediately clear to me. I shall start from 1st principles.

I recall now that we discussed the Pareto distribution by its tail function:

$$\mathrm{Pr}(X > b) = \frac{1}{b}$$

This seems to correspond to the "standard" Pareto distribution: with parametres $α = 1$, $θ = 1$.

We can take this tail function, and apply it to a discrete distribution. Keep in mind that we might need to scale the probabilities in the end.

### 1st attempt

$$\begin{align*}
\mathrm{Pr}(X = b) & = \mathrm{Pr}(X > b-1) - \mathrm{Pr}(X > b) \\
& = \frac{1}{b-1} - \frac{1}{b}
\end{align*}$$

In this case, $\mathrm{Pr}(X = 1)$ is unfortunately undefined, so let's shift everything by 1

### 2nd attempt

$$\mathrm{Pr}(X = b) = \frac{1}{b} - \frac{1}{b+1}$$

Here, $\mathrm{Pr}(X = 1) = 1 - \frac{1}{2}$; $\mathrm{Pr}(X = 2) = \frac{1}{2} - \frac{1}{3}$. It seems conveniently that the probabilities already sum to 1.

Now, Pareto has the "nice" property of having a mean of infinity. Do we still preserve that?

$$
\mathrm{Pr}(X = b) = \frac{1}{b} - \frac{1}{b+1} = \frac{1}{b(b+1)} \\
\mathbb{E}[X] = \sum_b \frac{b}{b(b+1)} = \sum_b \frac{1}{b+1}
$$

It diverges! Nice.

This seems to be a simple distribution but I can't find what it's called. I'm sure it's already called something, but since I discovered it independently, it should be henceforth called "*whatever*-Zjack distribution".

## Formalise the Problem

Anyway, back to the original problem. If the player experience is distributed like Pareto or Whatever-Zjack, and I sample a random player, then immediately I get that the expectation is a super experienced player. This result varies according to the distribution. If I have a geometric distribution instead, then it would be $\frac{1}{p}$, a finite value.

The problem with that approach is that when I start a game, I don't sample a player from the entire playerbase ever. A lot of the less-experienced players have left the game forever, and I won't ever encounter them. This seems to suggest that the previous result is an underestimate.

Another problem is time. Player experience changes over time. A player with a theoretical experience of 9999 will eventually reach 9999 games before quitting, but they might have just joined yesterday, and only currently have an experience of 2. Meanwhile, a lot of new players join every day.

So a better formulation of the original question is:

Every game, N new players join the game. Each player plays X games before quitting, where X follows some distribution. If I sample a random player who hasn't quitted yet, what's their expected experience?

This is still not fully / well defined. First, we didn't say how this universe started. Does the starting condition affect the result? Second, when do we sample a random player? Does the result depend on the time? And third, the number of players might diverge, or be stuck at 0. Should that be somehow accounted for?

## Simulation

These are non-trivial questions, so I shall resort to running a simulation, and see what comes out of it.

* At the beginning, I will generate N total players. Each will have a "lifetime" that follows some distribution D.
* I will sample 1 player, to see their current experience. This is the main result that I care about.
* After every round, all players accumulate 1 game of experience. Those who have reached their lifetime will "die".
* Before the next round starts, I will create some new players with new lifetime. I will keep the total number of players constant, so if 11 players died, I will generate 11 new players. This would be a different number each round.

I think it's a good idea to keep the total number of players (roughly) constant. Maybe an alternative approach is to regenerate X players, where X is the expected deaths each round, but that requires me to know what the expected death is. I could potentially do that for version 2.

The only parametres are N and D. I could see how the player experience I sample varies over time, and if it converges.

I could also change how the game starts, but I can't think of a reasonable alternative off the top of my head.

## Programming...

I wrote some code, using Julia and Pluto notebook. It is published here as [lai_feng_01.jl.html](https://mattzjack.gitlab.io/myuniverse/lai_feng_01.jl.html)

## Insights

Some interesting findings:

* The inverse of the CDF of the Whatever-Zjack distribution is simply $\lfloor \frac{1}{y} \rfloor$! Take a random uniform number between 0 and 1, put it under 1, take the floor, and voila, you get a random Whatever-Zjack number.
* Using the strategy described above, with 99 players, over 999 rounds: the mode of the player experience is 0! You are most likely going to encounter new players, also known as, derogatorily, n00bs. The median is a 17-game-old player. The mean is 87 games. Under this model, the answer is: No, strangers are usually not very good (at least not very experienced).
* I changed the distribution to Geometric(1/16). The results are similar: the mode is some small number, the median is 11, and the mean is 15.

I ran out of steam before implementing the version 2 mentioned above. I guess next time I lose to strangers on internet games, I should reflect a bit more on my skills before blaming the player base.