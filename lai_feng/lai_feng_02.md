# 來風第二

甲辰年六月

## Rounding is Unstable!

Or it's non-transitive. I'm not sure what the most precise description for it ought to be, but here's what I mean:

Suppose you round 1,4445 to 2 decimal places. That gives you 1,44.

Suppose, now, you first round 1,4445 to 3 decimal places, then round the result to 2 decimal places. Normally, I should expect that to give me the same answer as before, right?

Rounding to 3 decimal places gives me 1,445, and rounding the result to 2 decimal places gives me 1,45. This is not the same result, and the latter result is wrong!

Take another example. Still starting from 1,4445, rounding to an integer directly gives me 1. However, if I round to 3 then 2 then 1 decimal place, then finally to an integer, I get 1,445 -> 1,45 -> 1,5 -> 2. Very bad!

Maybe we should have this rule:

**You shall not round a rounded number again. You shall always start from the original number.**

However, this is quite impossible. We rarely have the "precise" / original number. When reading from a machine, it's already a rounded number.

So perhaps the rule should be:

**You shall not round off 1 digit. You shall always round off at least 2 digits.**

Let me attempt to prove / disprove that this rule can get rid of instability.

Let's simplify the problem. Suppose I have a list of $n$ digits: $[z_1, z_2, z_3, \ldots, z_n]$. Rounding to $m$ digits means I want to only keep $m$ digits, discarding the last $n - m$ digits. Meanwhile, I need to add 1 to the result, **iff** the last $n - m$ digits are larger than or equal to $[5, 0, 0, \ldots, 0]$.

Now, suppose I have my digits as ,14950. Rounding to 1 digit gives me ,1. Rounding off 2 digits then another 2 digits: ,150 -> ,2. Still unstable! This is hopeless!

It seems in general that the problem occurs near the border. If you see 4, 5, 49, 50, &c., you should be extra careful. Otherwise, it seems ok.

I accept defeat and declare the rule of defeat:

**Rounding is unstable. Be very careful when you see patters like 499 or 500, as they can be incorrect. The last digit of the rounded value should be treated as imprecise, with ± 1.**
