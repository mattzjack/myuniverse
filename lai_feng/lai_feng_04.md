# 來風第四

甲辰年寒月

I thought I found my latest pet peeve: Chinese names for byte units are misnomers?

The official / conventional names are:

* kilobyte ($10^3$): 千字節
* megabyte ($10^6$): 兆字節（？？）
* gigabyte ($10^9$): 千兆字節（？？） or 吉字節
* terabyte ($10^{12}$): 太字節 or 兆字節 (Taiwan)

My understanding has been, in Chinese, the prefixes are:
* 千: $10^3$
* 萬: $10^4$
* 億: $10^8$
* 兆: $10^{12}$ (*)
* 京: $10^{16}$ (*)

The first 3 should be unequivocal across many East Asian cultures. However, it appears the last 2 (*) were never well defined. According to Wikipedia:

> 《數術記遺》及《五經算術》：按黃帝為法，數有十等。及其用也，乃有三焉。\
> 十等者，謂「億、兆、京、垓、秭、穰、溝、澗、正、載」也。\
> 三等者，謂「上、中、下」也。\
> 其下數者，十十變之。若言十萬曰億，十億曰兆，十兆曰京也。\
> 　中數者，萬萬變之。若言萬萬曰億，萬萬億曰兆，萬萬兆曰京也。\
> 　上數者，數窮則變。若言萬萬曰億，億億曰兆、兆兆曰京也。

Well, in any case, 兆 should never mean $10^6$. Someone just took 兆 and forcefully fit it into the metric prefix system. If anything, it makes it sound like more than it is. I blame marketing.

I like the Taiwan names better:
| English | how many bytes | Taiwan name | note |
|--|--|--|--|
| kilobyte | $10^3$    | 千字節 | same in mainland |
| megabyte | $10^6$    | 百萬字節 | correct! |
| gigabyte | $10^9$    | 吉字節 | transliterative (sound-alike) |
| terabyte | $10^{12}$ | 兆字節 | correct! but conflicts with mainland name of a different meaning |
| petabyte | $10^{15}$ | 拍字節 | transliterative; same in mainland |

On the topic of pet peeve, TIME ZONES!!! I keep receiving messages that uses `_ST` or `_DT` incorrectly. Unacceptable.

<sup><sub>_future: GSM / CDMA_</sub></sup>