# 寶鏡第一

甲辰年寒月

## 來風，溪漁

Finally thinking through the difference between 來風 and 溪漁 for myself.

- 溪漁 is structured and educational. Likely more serious topics like economics.
- 來風 is less structured and more random. More technical / coding / computer stuff that normal people don't need to care about.

## Causing Problems, Fixing Problems

I am causing a lot of problems for myself, like using Linux. I don't really know why. I think it is because I prefer to understand the inner workings of things. When something fails, I really want to know why it fails. Of course, when something never fails, I happily continue to use it. Windows or Mac does a good job with their bugs, so I should be happy using them. Except when I want a very specific customisation. Also, I subscribe to open-source. These come from my perfectionist / minimalist tendencies.

This ends up costing me a lot of time and energy. Knowledge is power. But I am likely not finding the balance where the marginal benefit of studying a niche topic is equal to the marginal benefit of doing something else with that time. "Benefit", mostly in the sense of material gain, excluding my brain's (temporary) enjoyment. It's almost like an addiction.

## Minimalism Requires Maximalism

A minimal lifestyle requires a large amount of effort / resources. Say I want an extremely clean looking apartment. I need to have a lot of space to tuck things away. I need to be able to live off of store-bought food so that I can avoid kitchen clutter.

On the flip side, to live frugally, I need to have a lot of stuff around. I need tools to fix this and that. That's also minimalism -- by consuming little. The "minimalism" I envision when I think about that word is one with nothing in sight and everything looks clean. That's hard to achieve frugally; it's likely expensive. Is frugal not minimalism?

To achieve minimalism, you will have to raise the entropy somewhere else. That's why I always have a pile of "garbage" (unorganised stuff) on my "sacrifice-desk" which is next to my "clean-desk". There is probably a balance here, too. Average out the entropy and then try to minimal-ise.

The same goes for tech. Mac is very clean. But a lot of plumbing happens behind the scene. Linux is very minimal, but you need to do a lot of work to make it usable.

That's not to say there's no pure bloat. For me, for example, transitional animation is mostly pure bloat. They are not clean and they require work. They slow down the system while costing resources. However, I think transitional animation on touch device gestures are fine, to an extent, because they are more cognitively consistent. That goes under the category of "pleasing at the cost of plumbing under the hood". Launch animation from touching an app icon is pure bloat, though, according to me. There's no clearly defined line, it seems.

Thinking about minimalism also takes time and energy. I like to remove unnecessary apps from my devices, or remove unnecessary items from my life. Thinking about apps takes time. I sometimes remove an app but then later need it, causing some headache, like trying to connect to public wifi to download it. Removing items also takes time, as I don't like to waste. I try to donate it to Goodwill or sell it online, taking even more time, and possibly defeating the purpose of reducing waste. For example, when I ship something over eBay, I create unnecessary shipping. It may be better to just toss the thing in garbage.

Price could be a good indicator. Under various ideal assumptions, the price reflects the social cost of an action. If I sell an old electronic for a profit that I deem worthy of my time, that should be socially optimal. The price of shipping should reflect the marginal resources used by shipping. The buyer happily paid for my item and shipping, and everyone is weakly happier. However, that's all assuming price is perfectly set.

If we can assume that, we should, then, try to maximise our asset / wallet, and minimise spending. For example, if beef is cheaper than impossible meat, we should just buy beef and consider that to be less socially costly than impossible meat, taking environment into consideration. But markets are clearly imperfect. Many things are not factored into the price. A trivial example is if I steal money from someone and don't get caught. There are also factors like economy of scale, or imperfect equilibriums.

## no conclusion

Try to think about the total social cost of actions. Think about what may or may not have been factored into the price. But it's not recommended to do to that as individuals, because who's going to compensate you, even if you raised total social welfare? Just do whatever you think is right. That's why we need economists to try to align people's selfish incentives with what's beneficial for socienty as a whole.