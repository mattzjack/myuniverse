# 溪漁第二

甲辰年七月

In the context of economics and randomised controlled trials, I've always found recondite the differences among jargons like ATE, ATT, ITT, &c.

Here's my understanding. Hopefully it is correct and helpful.

## **Average Treatment Effect** (ATE)

average of the effect of the treatment (duh). This should be among the entire population. This would be:

$$
\mathrm{Avg}_\text{each person} [ \text{Outcome}|\text{treated} - \text{Outcome}|\text{untreated} ]
$$

This is unobservable due to counterfactual, but it can be estimated. Suppose we treat a random sample of the population (with full compliance), and don't treat another random sample (full compliance). The difference of their average outcome would be a good estimate of the average treatment effect, under various assumptions.

It seems that this is a general, umbrella term. It doesn't explicitly lay out assumption about compliance or population.

## **(Average) Intent-To-Treat Effect** (ITT)

the causal effect of intent to treat. This is the most straightforward to me. Suppose we assign a random sample to treatment group, and another random sample to control group, and there is non-compliance, meaning some in the treatment group end up untreated, & vice versa. The (average) intent-to-treat effect is simply the difference of average outcome between the assigned-treatment group and the assigned-control group, essentially eschewing the issue of non-compliance. This is analogous to the reduced form of the instrumental variable estimation, where assignment is the instrument (& actual treatment or the lack thereof is the regressor). This would be:

$$
\mathrm{Avg} [ \text{Outcome}|\text{assigned treatment} ] - \mathrm{Avg} [ \text{Outcome}|\text{assigned control} ]
$$

For example, a doctor encourages some patients to exercise, and observes their health. The intent-to-treat effect is the effect of the encouragement on health (rather than the effect of exercise on health).

Note that with full compliance, the average intent-to-treat effect is the same as the average treatment effect.

### **A word on non-compliance**

There are 4 groups of people:

* Complier: takes treatment iff assigned to treatment group
* Always-Taker
* Never-Taker
* Defier: takes treatment iff assigned to the control group

It seems that we normally assume there're no defiers, because they are weird (-: and uncommon.

In both treatment and control groups, we can get a mix of all 3 types.

In a world / under the assumption of one-sided non-compliance, no one is treated in the control group. This means:

* in the treatment group, we still get a mix of all 3 types
* in the control group, there're only compliers and never-takers. There're no always-takers in the control group.

Perfect compliance means everyone in the treatment group is treated, and no one in the control group is treated. This doesn't imply everyone is a complier. There can be always-takers in the treatment group, and never-takers in the control group. However, random assignment should ensure that treatment and control groups consist of the same types of people. Therefore, under random assignment, perfect compliance should imply there are only compliers among both groups.

In the same vein, under random assignment, one-sided noncompliance implies that there are only compliers and never takers among both groups.

## **Local Average Treatment effect** (LATE, Complier Average Causal Effect / CACE)

the average treatment effect among compliers. Following the analogy above, this would be the instrumental variable estimate. *I.e.*:

$$
 \frac{\mathrm{Avg}[ \text{Outcome}|\text{assigned treatment} ] - \mathrm{Avg}[ \text{Outcome}|\text{assigned control} ]}
 {\mathrm{Avg}[ \text{actually treated}|\text{assigned treatment} ] - \mathrm{Avg}[ \text{actually treated}|\text{assigned control} ]}
$$

This should be valid for both one-sided and two-sided noncompliance.

| outcome   | compliers | always-takers | never-takers |
|-----------|-----------|---------------|--------------|
| treatment | y+δ       | y+δ           | y            |
| control   | y         | y+δ           | y            |

Assuming the share of each of 3 types are the same, you can see that we recover δ with our formula.

This is the average treatment effect, local to compliers only. In other words, assuming everyone is a complier, this would be the average treatment effect.

In the doctor example, this estimates the effect of exercising on health.

In theory, there might be some effect on always-takers, never-takers, or defiers. *E.g.*, there's a counterfactual where an always-taker is forced to not take the treatment. However, we can't observe that with our experiment. The local average treatmente effect only estimates the effect for compliers.

## **Average Treatment effect On Treated** (ATT, TOT)

the average treatment effect among those who are treated. The difference between the average treatmente effect on treated and the local average treatment effect seems subtle: the former is among treated, and the latter is among compliers.

In one-sided non-compliance, there're no always-takers or defiers. The only treated people are compliers in the treatment group. What's their counterfactual? They are compliers, so it would be compliers in the control group. In this case, the average treatmente effect on treated should be equivalent to the local average treatment effect.

| outcome   | compliers       | never-takers    |
|-----------|-----------------|-----------------|
| treatment | y+δ (treated)   | y (not treated) |
| control   | y (not treated) | y (not treated) |

## more info

Here's one webpage for more info: https://www.povertyactionlab.org/resource/data-analysis
